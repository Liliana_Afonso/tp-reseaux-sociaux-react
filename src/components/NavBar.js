import React from 'react';
import { Link } from "react-router-dom";
import styled from 'styled-components';


const Navbar = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 25px;
`

const Logo = styled.div`
    display: flex;
    flex-grow: 1;
`

const FlexRight = styled.div`
    width: 120px;
    display: flex;
    justify-content: space-between;
    margin-right:25px
`

const Img = styled.img`
    width: 50px;
    padding: 10px 0px;
`
const Button = styled.button`
    background: transparent;
    border: none;
    font-size:1rem;

`

const NavBar = () => {
    const LogOut = () => {
        localStorage.clear();
        document.location.href = "/Login";
    }

    return (
        <Navbar className="navBar">
            <Logo>
                <Link to="/"><h1 className="Logo">PostBook</h1></Link>
            </Logo>
            <FlexRight>
                <Link to="/profile/" style={{ display: 'flex' }}><Img src={require("../assets/user.svg")} alt="" /></Link>
                <Button to="/login/" onClick={() => LogOut()} title="Déconnexion"><Img src={require("../assets/turn-off.svg")} alt="" /></Button>
            </FlexRight>
        </Navbar>
    );
}

export default NavBar;