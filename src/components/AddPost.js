import React, { useState, useContext } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import ApiContext from '../contexts/ApiContext';

const Input = styled.input`
    height: 40px;
    padding: 5px 10px;
    margin-bottom:10px;
`

const Textarea = styled.textarea`
    height:100px;
    padding: 5px 10px;
    margin-bottom:10px;
`

const Form = styled.form`
    display:flex;
    flex-direction: column;
    align-content; flex-end;
    width: 70%;
    height: 20vh;
`

const Button = styled.button`
    margin: 10px 0px 5px;
    width:130px;
    background : #F8D66C;
    padding: 10px;
    border: none;
    border-radius:20px;
    align-self: center;
`

const FlexRight = styled.div`
    display: flex;
    justify-content: flex-end;
`

const AddPost = () => {

    const [title, setTitle] = useState([]);
    const [content, setContent] = useState([]);
    const [error, setError] = useState([]);
    const { jwt, user, setUser } = useContext(ApiContext);

    const AddPost = () => {
        axios.post('https://strapi-crea.5ika.org/posts',
            {
                title,
                content,
                user
            },
            {
                headers: { Authorization: `Bearer ${jwt}` }
            }
        ).then(response => {
            const fetchedData = response.data;
            setUser(fetchedData);
        })
            .catch((err) => setError(error));
    }

    const onSubmit = (e) => {
        e.preventDefault();
        console.log("user: " + user.username);
        AddPost();
        setTitle("");
        setContent("");
        window.location.reload(false); // pour pouvoir voir mon poste

    };

    return (
        <div className="addPost">
            <Form>
                <Input
                    type="text"
                    name="addTitle"
                    placeholder="Titre du poste"
                    value={title}
                    error={error?.field === 'title'}
                    onChange={(e) => setTitle(e.target.value)}
                    required
                />
                <Textarea
                    type="text"
                    name="addContent"
                    placeholder="Ecrivez votre poste"
                    value={content}
                    error={error?.field === 'content'}
                    onChange={(e) => setContent(e.target.value)}
                    required

                />
                <FlexRight>
                    <Button className="btn" onClick={onSubmit} >Ajouter </Button>
                </FlexRight>

            </Form>
        </div>
    );
}

export default AddPost;