import React, { useEffect, useContext } from 'react';
import axios from 'axios';
import ApiContext from '../contexts/ApiContext';
import styled from 'styled-components';
import { Link } from "react-router-dom";

const List = styled.div`
    display:flex;
    flex-direction: column;
    text-align: initial;
    margin: 50px 0px 50px;
    width:70%;
    height: 40vh;
    overflow-y: scroll;
`

const LiContent = styled.li`
    margin-bottom:10px;
    text-align: justify;
`

const LiAuthor = styled.li`
    margin-bottom:30px;
    float:right;
    font-style: italic;
`

const LiTitle = styled.li`
    margin-bottom:10px;
    text-transform: uppercase;
    font-weight : bold;
`

const Post = styled.ul`
    padding-top: 10px;
    border-top: solid 1px #F8D66C;
    list-style-type: none;
    padding : 10px 15px;

`

const ListPosts = () => {
    const { jwt, posts, setPosts, error, setError } = useContext(ApiContext);

    useEffect(() => {
        axios.get('https://strapi-crea.5ika.org/posts',
            {
                headers: { Authorization: `Bearer ${jwt}` }
            }
        ).then(response => {
            const fetchedData = response.data;
            setPosts(fetchedData);
        })
            .catch((err) => setError(error));
        ;
    }, [error, jwt, setPosts, setError])

    return (
        <div className="listPosts">
            <h2>Liste des posts</h2>
            <List>
                {posts.map((post) => (
                    <Link to={`/post/${post.id}`} >
                        <Post>
                            <LiTitle key={post.index}> {post.title}</LiTitle>
                            <LiContent key={post.index}> {post.content}</LiContent>
                            {!post.user === "" ?
                                (
                                    <LiAuthor key={post.index}> {post.user.username}</LiAuthor>

                                ) : (<LiAuthor key={post.index} >Anonyme</LiAuthor>)
                            }
                        </Post>
                    </Link>
                ))}
            </List>
        </div >
    );
}

export default ListPosts;