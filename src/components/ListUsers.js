import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import ApiContext from '../contexts/ApiContext';
import styled from 'styled-components';
import { Link } from "react-router-dom";

const Aside = styled.aside`
    width: 25%;
    height: 100vh;
    padding: 30px;
    border-left: 3px solid #F8D66C;
    float: right;
`

const List = styled.ul`
    display:flex;
    flex-direction: column;
    list-style-type: none;
    text-align: initial;
    margin: 20px 0px;
    height: 80vh;
    overflow-y: scroll;
`

const Li = styled.li`
    margin-top:20px;
`

const H2 = styled.h2`
    text-align: initial;
`

const ListUsers = ({ user }) => {
    const [users, setUsers] = useState([]);
    const [error, setError] = useState([]);
    const { jwt } = useContext(ApiContext);

    useEffect(() => {
        axios.get('https://strapi-crea.5ika.org/users',
            {
                headers: { Authorization: `Bearer ${jwt}` }
            }
        ).then(response => {
            const fetchedData = response.data;
            setUsers(fetchedData);
        })
            .catch((err) => setError(error));
    }, [jwt, error])

    return (
        <Aside className="listUsers">
            <H2>Liste des users</H2>
            <List>
                {users.map((user, index) =>
                    <Li key={index}> <Link to={`/user/${user.id}`}>{user.username} </Link> </Li>
                )}
            </List>

        </Aside>
    );
}

export default ListUsers;