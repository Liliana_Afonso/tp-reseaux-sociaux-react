import React, { useContext, useState } from 'react';
import NavBar from '../components/NavBar';
import ApiContext from '../contexts/ApiContext';
import styled from 'styled-components';
import axios from 'axios';
import { useHistory } from "react-router-dom";

const Form = styled.form`
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    align-items: center;
`

const Input = styled.input`
    width:300px;
    height:40px;
    padding: 5px 10px;
`
const Button = styled.button`
    margin: 40px 0px 5px;
    width:130px;
    background : #F8D66C;
    padding: 10px;
    border: none;
    border-radius:20px;
    align-self: center;
`

const Label = styled.label`
    margin :20px 0px 5px;
    text-align:left;
`

const Textarea = styled.textarea`
    height:100px;
    width:300px;
    padding: 5px 10px;
`

const Profile = () => {
    let history = useHistory();

    const { user, setUser, jwt, error, setError } = useContext(ApiContext);
    const [firstname, setFirstname] = useState(user.firstname);
    const [username, setUsername] = useState(user.username);
    const [lastname, setLastname] = useState(user.lastname);
    const [email, setEmail] = useState(user.email);
    const [bio, setBio] = useState(user.bio);

    const getUser = () => {

        axios.put(`https://strapi-crea.5ika.org/users/${user.id}`,
            {
                username,
                email,
                firstname,
                lastname,
                bio,
            },
            {
                headers: { Authorization: `Bearer ${jwt}` }
            }
        ).then(response => {
            const fetchedData = response.data;
            setUser(fetchedData);

        })
            .catch((err) => setError(error));
    }

    const Delete = () => {
        if (window.confirm("Etes-vous sûre de vouloir supprimer le compte ? ")) {
            localStorage.clear();
            history.push("/login");

            axios.delete(`https://strapi-crea.5ika.org/users/${user.id}`,
                {
                    headers: { Authorization: `Bearer ${jwt}` }
                }
            ).then(
                response => {
                    const fetchedData = response.data;
                    setUser(fetchedData);
                    localStorage.clear();
                    window.location.reload(false);

                })
                .catch((err) => setError(error));
        } else {
            history.push("/profile");
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        getUser();
    };


    return (
        <div className="profile">
            <NavBar />

            <h2>Mon profil</h2>
            <Form>
                <Label htmlFor="username"> Nom d'utilisateur  </Label>
                <Input
                    type="text"
                    name="username"
                    value={username}
                    error={error?.field === 'username'}
                    onChange={(e) => setUsername(e.target.value)}
                />
                <Label htmlFor="firstname"> Prénom </Label>
                <Input
                    type="text"
                    name="firstname"
                    value={firstname}
                    error={error?.field === 'firstname'}
                    onChange={(e) => setFirstname(e.target.value)}
                />
                <Label htmlFor="lastname"> Nom  </Label>
                <Input
                    type="text"
                    name="lastname"
                    error={error?.field === 'lastname'}
                    value={lastname}
                    onChange={(e) => setLastname(e.target.value)}
                />
                <Label htmlFor="lastname"> Email  </Label>
                <Input
                    type="text"
                    name="email"
                    error={error?.field === 'email'}
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <Label htmlFor="bio"> Biographie </Label>
                <Textarea
                    type="text"
                    name="bio"
                    error={error?.field === 'bio'}
                    value={bio}
                    onChange={(e) => setBio(e.target.value)}
                >
                </Textarea>
                <Button onClick={onSubmit}> Enregistrer</Button>
                <Button onClick={Delete}> Supprimer compte</Button>
            </Form>
        </div>
    );
}

export default Profile;