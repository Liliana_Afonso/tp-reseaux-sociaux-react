import React, { useContext } from 'react';
import { Link } from "react-router-dom";
import ApiContext from '../contexts/ApiContext';
import styled from 'styled-components';
import '../App.css';


const Block = styled.div`
    display: flex;
    flex-direction: column;
    justify-content:center;
    margin: 100px auto;
    width:500px;
    padding: 50px;
    background-color: #FFF;
`

const Form = styled.form`
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    align-item : center;
`
const Label = styled.label`
    margin :20px 0px 5px;
    text-align:left;
`

const Input = styled.input`
    width:300px;
    height: 40px;
    padding: 5px 10px;
`

const Button = styled.button`
    margin: 40px 0px 5px;
    width:130px;
    background : #F8D66C;
    padding: 10px;
    border-color: transparent;
    border-radius:20px;
    align-self: center;
`

const Login = () => {
    const { login, username, setUsername, password, setPassword, error, setError } = useContext(ApiContext);

    const onSubmit = (e) => {
        e.preventDefault();
        setError(null);
        if (!username || username === '')
            setError({ field: 'username', message: "Merci d'entrer votre identifiant" });
        else if (!password || password === '')
            setError({ field: 'password', message: "Merci d'entrer un mot de passe valide" });
        else
            login();
    };

    return (
        <Block className="login">
            <h2>Connexion</h2>

            <Form>
                <Label htmlFor="username"> Nom d'utilisateur : </Label>

                <Input
                    className="input"
                    type="text"
                    name="username"
                    placeholder="nom d'utilisateur"
                    value={username}
                    error={error?.field === 'username'}
                    onChange={(e) => setUsername(e.target.value)}
                    required
                />

                <Label htmlFor="password"> Mot de passe : </Label>
                <Input
                    className="input"
                    type="password"
                    name="password"
                    placeholder="mot de passe"
                    value={password}
                    error={error?.field === 'password'}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
                <Button className="btn" onClick={onSubmit} >Se connecter </Button>
                <Link to="/sign_up/">Créer un compte ?</Link>
            </Form>
        </Block>
    );
}

export default Login;