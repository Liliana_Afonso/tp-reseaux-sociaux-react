import React from 'react';
import ListUsers from '../components/ListUsers';
import NavBar from '../components/NavBar';
import ListPosts from '../components/ListPosts';
import AddPost from '../components/AddPost';

const Home = () => {
    return (
        <div className="home ">
            <ListUsers></ListUsers>
            <NavBar />
            <ListPosts></ListPosts>
            <AddPost></AddPost>
        </div>
    );
}

export default Home;