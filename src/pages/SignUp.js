import React, { useContext, useState } from 'react';
import { Link, useHistory } from "react-router-dom";
import ApiContext from '../contexts/ApiContext';
import axios from 'axios';
import styled from 'styled-components';
import '../App.css';

const Block = styled.div`
    display: flex;
    flex-direction: column;
    justify-content:center;
    margin: 100px auto;
    width:500px;
    padding: 50px;
    background-color: #FFF;
`

const Form = styled.form`
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    align-item : center;
`

const Input = styled.input`
    width:300px;
    height: 40px;
    padding: 5px 10px;
`

const Label = styled.label`
    margin :20px 0px 5px;
    text-align:left;
`

const Textarea = styled.textarea`
    height:100px;
    padding: 5px 10px;
`

const Button = styled.button`
    margin: 40px 0px 5px;
    width:130px;
    background : #F8D66C;
    padding: 10px;
    border: none;
    border-radius:20px;
    align-self: center;
`

const SignUp = () => {
    let history = useHistory();

    const { error, setError, setJwt, setUser, password, setPassword, username, setUsername } = useContext(ApiContext);

    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [bio, setBio] = useState('');

    const createUser = () => {
        axios.post('https://strapi-crea.5ika.org/auth/local/register',
            {
                username,
                password,
                email,
                firstname,
                lastname,
                bio
            },
        )
            .then(response => {
                setJwt(response.data.jwt);
                setUser(response.data.user);

                localStorage.setItem("JWT", response.data.jwt)
                localStorage.setItem("UserConnected", JSON.stringify(response.data.user))
                history.push("/");

            })
            .catch((err) => setError(error));
    }

    const onSubmit = (e) => {
        e.preventDefault();
        createUser();
    };

    return (

        <Block className="signUp">

            <h2>Créer un compte</h2>

            <Form>
                <Label htmlFor="firstname"> Prénom : </Label>
                <Input
                    type="text"
                    name="firstname"
                    value={firstname}
                    error={error?.field === 'firstname'}
                    onChange={(e) => setFirstname(e.target.value)}
                    required
                />

                <Label htmlFor="lastname"> Nom : </Label>
                <Input
                    type="text"
                    name="lastname"
                    value={lastname}
                    error={error?.field === 'lastname'}
                    onChange={(e) => setLastname(e.target.value)}
                    required
                />

                <Label htmlFor="username"> Nom d'utilisateur : </Label>
                <Input
                    type="text"
                    name="username"
                    value={username}
                    error={error?.field === 'username'}
                    onChange={(e) => setUsername(e.target.value)}
                    required
                />

                <Label htmlFor="email"> Email : </Label>
                <Input
                    type="text"
                    name="email"
                    value={email}
                    error={error?.field === 'email'}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />

                <Label htmlFor="password"> Mot de passe : </Label>
                <Input
                    type="password"
                    name="password"
                    value={password}
                    error={error?.field === 'password'}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />

                <Label htmlFor="bio"> Biographie : </Label>
                <Textarea
                    className="input"
                    type="text"
                    name="bio"
                    value={bio}
                    error={error?.field === 'bio'}
                    onChange={(e) => setBio(e.target.value)}
                    required
                />

                <Button className="btn" onClick={onSubmit} >Créer un compte </Button>
                <Link to="/login/">Déjà un compte ? </Link>
            </Form>
        </Block>
    );
}

export default SignUp;