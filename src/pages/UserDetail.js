import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import ApiContext from '../contexts/ApiContext';
import styled from 'styled-components';
import NavBar from '../components/NavBar';

const PostShow = styled.div`
    margin-top: 50px;
    text-align: justify;
    border-top: solid 1px #F8D66C;
    padding-top:10px;
`

const Table = styled.table`
    text-align: left;
    margin:10px 0px 0px;
    background:#F8D66C;
    padding: 20px;
    padding-right: 200px
`

const Td = styled.td`
    padding-left: 50px;
`

const H3 = styled.h3`
    text-align: left;
    margin-top:50px;
    color: #F8D66C;
`

const Post = ({ match }) => {
    const { jwt, error, setError, posts } = useContext(ApiContext);
    const [userPost, setUserPost] = useState('');

    let { id } = match.params;

    useEffect(() => {
        axios.get(`https://strapi-crea.5ika.org/users/${id}`,
            {
                headers: { Authorization: `Bearer ${jwt}` }
            }
        ).then(response => {
            const fetchedData = response.data;
            setUserPost(fetchedData);
        })
            .catch((err) => setError(error));
    }, [id, jwt, error, setError])

    return (
        <div className="post" style={{ marginBottom: '50px' }}>
            <NavBar />
            <h2>Page de {userPost.username}</h2>
            <H3>Informations Personnelles</H3>
            <Table>
                <tr>
                    <th>Prénom</th>
                    <Td>{userPost.firstname}</Td>
                </tr>
                <tr>
                    <th>Nom</th>
                    <Td>{userPost.lastname}</Td>
                </tr>
                <tr>
                    <th>Email</th>
                    <Td>{userPost.email}</Td>
                </tr>
                <tr>
                    <th>Biographie</th>
                    <Td>{userPost.bio}</Td>
                </tr>
            </Table>
            <H3 className="mb-neg">Posts Publiés </H3>
            {posts.map((post) => (
                <>
                    {!post.user == "" ?
                        (
                            <>
                                {id == post.user.id ? (
                                    <>
                                        <PostShow>
                                            <h3 key={post.index}>{post.title}</h3>
                                            <p key={post.index}>{post.content}</p>
                                        </PostShow>
                                    </>
                                ) : ("")
                                }
                            </>
                        ) : ("")
                    }
                </>
            ))
            }
        </div >
    );
}

export default Post;