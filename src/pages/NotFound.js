import React from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components';

const Block = styled.div`
    display: flex;
    flex-direction: column;
    justify-content:center;
    margin: 200px auto;
    width:500px;
    padding: 100px;
    border: 2px solid #F8D66C;
`

const NotFound = () => {
    return (
        <Block>
            <h1>Erreur 404 ou 401</h1>
            <Link style={{ marginTop: '20px', borderBottom: '1px solid #F8D66C', paddingBottom: '10px' }} to="/login/"> Se connecté ? </Link>
            <Link style={{ marginTop: '10px' }} to="/"> Retour à la page d'accueil ? </Link>

        </Block>
    );
};

export default NotFound;
