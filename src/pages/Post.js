import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import ApiContext from '../contexts/ApiContext';
import styled from 'styled-components';
import NavBar from '../components/NavBar';

const PostShow = styled.div`
    margin-top: 50px;
    text-align: center;
`
const Content = styled.p`
    width: 80%;
    margin: 20px auto;
`

const Post = ({ match }) => {
    const { jwt, error, setError } = useContext(ApiContext);
    const [post, setPost] = useState("");

    let { id } = match.params;

    useEffect(() => {
        axios.get(`https://strapi-crea.5ika.org/posts/${id}`,
            {
                headers: { Authorization: `Bearer ${jwt}` }
            }
        ).then(response => {
            const fetchedData = response.data;
            setPost(fetchedData);
        })
            .catch((err) => setError(error));
    }, [jwt, id, error, setError, setPost])

    return (
        <div className="post">
            <NavBar />
            <PostShow>
                <h2 >{post.title}</h2>
                <Content>{post.content}</Content>
                {/* <p>Ecrit par : {post.user.username}</p> */}
            </PostShow>
        </div>
    );
}

export default Post;