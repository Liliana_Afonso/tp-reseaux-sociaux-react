import React, { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios';
import Login from './pages/Login';
import Profile from './pages/Profile';
import SignUp from './pages/SignUp';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NotFound from './pages/NotFound';
import UserDetail from './pages/UserDetail';
import Post from './pages/Post';
import Home from './pages/Home';
import ApiContext from './contexts/ApiContext';


function App() {
  const [jwt, setJwt] = useState('');
  const [user, setUser] = useState([]); // que mon identifiant
  const [error, setError] = useState();
  // const [users, setUsers] = useState([]);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [posts, setPosts] = useState([]);

  const login = () => {

    axios.post('https://strapi-crea.5ika.org/auth/local',
      {
        identifier: username,
        password: password,

      },
    ).then(response => {
      setJwt(response.data.jwt);
      setUser(response.data.user);
      setUsername(response.data.username);
      setPassword(response.data.password);

      localStorage.setItem("JWT", JSON.stringify(response.data.jwt))
      localStorage.setItem("UserConnected", JSON.stringify(response.data.user))
      document.location.href = "/";

    })
      .catch((err) => setError(error));
  }

  useEffect(() => {

    const jwtStorage = localStorage.getItem("JWT");
    if (jwtStorage) {
      setJwt(JSON.parse(jwtStorage));
    }
    const userStorage = localStorage.getItem("UserConnected");
    if (userStorage) {
      setUser(JSON.parse(userStorage));
    }

  }, [])

  const contextValue = {
    login,
    username,
    setUsername,
    password,
    setPassword,
    error,
    setError,
    // users,
    // setUsers,
    jwt,
    setJwt,
    user,
    setUser,
    posts,
    setPosts

  }

  return (
    <ApiContext.Provider value={contextValue}>
      <Router>
        <div className="App">
          {!jwt ?
            (
              <Switch>
                <Route path="/login" exact component={Login} />
                <Route path="/sign_up" exact component={SignUp} />
                <Route component={NotFound} />
              </Switch>
            ) : (
              < div className="container">
                <Switch>
                  <Route path="/" exact component={Home} />
                  <Route path="/user/:id" exact component={UserDetail} />
                  <Route path="/post/:id" exact component={Post} />
                  <Route path="/profile" exact component={Profile} />
                  <Route component={NotFound} />
                </Switch>
              </div>
            )
          }
        </div>
      </Router>
    </ApiContext.Provider>

  );
}

export default App;
